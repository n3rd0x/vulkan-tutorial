/**
 * Vulkan Tutorial
 * Ref:     https://vulkan-tutorial.com
 * Author:  T.Sang Tran
 */


// Local includes
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// GLM includes
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

// STL includes
#include <iostream>
#include <vector>


// ************************************************************
// Main
// ************************************************************
int main() {
    // Initialise GLFW and create a window.
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    GLFWwindow* window = glfwCreateWindow(800, 600, "Vulkan window", nullptr, nullptr);

    // Request available extensions.
    uint32_t numOfExts = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &numOfExts, nullptr);

    std::vector<VkExtensionProperties> exts(numOfExts);
    vkEnumerateInstanceExtensionProperties(nullptr, &numOfExts, exts.data());

    std::cout << "Available extensions (" << numOfExts << "):" << std::endl;
    for(const auto& e : exts) {
        std::cout << " * " << e.extensionName << std::endl;
    }

    // Test algebra.
    glm::mat4 mat;
    glm::vec4 vec;
    auto test = mat * vec;


    // Just run once.
    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        break;
    }

    // Clean GLFW.
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
