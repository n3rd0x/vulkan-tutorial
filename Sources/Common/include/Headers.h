/**
 * Vulkan Tutorial
 * Ref:     https://vulkan-tutorial.com
 * Author:  T.Sang Tran
 */


#ifndef _Headers_h_
#define _Headers_h_


// STL includes
#include <vector>


// Vulkan & GLFW includes
// TST 2018-06-17
// LunarSDK support Vulkan on Windows, Unix, Mac, IOS and Android.
// If GLFW is compiled with Vulkan support, we will let it include
// Vulkan headers by defining GLFW_INCLUDE_VULKAN
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>


// Validation layers are layers that can be attached into Vulkan function calls
// to apply additional operations such as:
//  * Checking the values of parameters against the specification to detect misuse
//  * Tracking creation and destruction of objects to find resource leaks
//  * Checking thread safety by tracking the threads that calls originate from
//  * Logging every call and its parameters to the standard output
//  * Tracing Vulkan calls for profiling and replaying
const std::vector<const char*> VALIDATION_LAYERS = {"VK_LAYER_KHRONOS_validation"};

// Device extensions.
// VK_KHR_SWAPCHAIN_EXTENSION_NAME  => VK_KHR_swapchain
const std::vector<const char*> DEVICE_EXTENSIONS = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};


#endif  // _Headers_h_
