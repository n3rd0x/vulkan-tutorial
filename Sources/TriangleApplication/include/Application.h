/**
 * Vulkan Tutorial
 * Ref:     https://vulkan-tutorial.com
 * Author:  T.Sang Tran
 */


#ifndef _Application_h_
#define _Application_h_


// Local includes
#include "Headers.h"

// STL includes
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

// GLM includes
#include <glm/glm.hpp>


/**
 * Vulkan application.
 */
class Application {
public:
    // ************************************************************
    // Structure Declarations
    // ************************************************************
    struct QueueFamilyIndices {
        int graphicsFamily = -1;
        int presentFamily  = -1;

        bool isComplete() {
            return (graphicsFamily >= 0) && (presentFamily >= 0);
        }
    };


    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };


    struct Vertex {
        glm::vec2 position;
        glm::vec3 colour;

        static std::array<VkVertexInputAttributeDescription, 2> getAttributeDescription();
        static VkVertexInputBindingDescription getBindingDescription();
    };




    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * Default constructor.
     */
    Application();


    /**
     * Default destructor.
     */
    ~Application();


    /**
     * Initiate main loop for this application.
     */
    void run();




    // ************************************************************
    // Static Member Declarations
    // ************************************************************
    static VkResult createDebugUtilsMessengerEXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT* createInfo,
        const VkAllocationCallbacks* allocator,
        VkDebugUtilsMessengerEXT* debugMessenger);


    static void destroyDebugUtilsMessengerEXT(
        VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* allocator);


    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
        void* userData);


    /**
     * @brief Callback when the window is resized.
     * @param window Current window.
     * @param width New width size.
     * @param height New height size.
     */
    static void framebufferResizedCallback(GLFWwindow* window, int width, int height);


    /**
     * @brief Read file.
     * @param filename File name to process.
     * @return List of readed characters.
     */
    static std::vector<char> readFile(const std::string& filename);


/**
 * Validation layer.
 */
#ifdef NDEBUG
    static const bool VALIDATION_SUPPORT = false;
#else
    static const bool VALIDATION_SUPPORT = true;
#endif


    /**
     * Window size.
     */
    static const int WINDOW_WIDTH  = 800;
    static const int WINDOW_HEIGHT = 600;


    /**
     * @brief Paths.
     */
    static const std::string PATH_SHADER;


    /**
     * @brief Vertices.
     */
    static const std::vector<Vertex> sVertices;


protected:
    // ************************************************************
    // Protected Member Declarations
    // ************************************************************
    /**
     * @brief Check device extension support.
     * @param device Device to process.
     * @return True if supported. Otherwise return false.
     */
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);


    /**
     * @brief Check wether the device is suitable.
     * @param device Device to check.
     * @return True on success, otherwise return false;
     */
    bool checkDeviceSuitable(VkPhysicalDevice device);


    /**
     * Check validation layer support.
     * @return True if layer is supported.
     */
    bool checkValidationLayerSupport();


    /**
     * @brief Choose extent.
     * @param capabilities Capabilities to check.
     * @return Suitable extent.
     */
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);


    /**
     * @brief Choose best present mode.
     * @param availableModes List of available present modes.
     * @return Best present mode.
     */
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availableModes);


    /**
     * @brief Choose best surface format.
     * @param availableFormats List of available formats.
     * @return Best surface format.
     */
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);


    /**
     * @brief Clean up.
     */
    void cleanUp();


    /**
     * @brief Clean up the swap chain.
     */
    void cleanUpSwapChain();


    /**
     * @brief Create command buffers.
     */
    void createCommandBuffers();


    /**
     * @brief Create command pool.
     */
    void createCommandPool();


    /**
     * @brief Create framebuffers.
     */
    void createFramebuffers();


    /**
     * @brief Create graphics pipeline.
     */
    void createGraphicsPipeline();


    /**
     * Create a Vulkan instance.
     */
    void createInstance();


    /**
     * @brief Create image views.
     */
    void createImageViews();


    /**
     * @brief Create logical device.
     */
    void createLogicalDevice();


    /**
     * @brief Create render pass.
     */
    void createRenderPass();


    /**
     * @brief Create shader model.
     * @param code List of code characters.
     * @return Shader model object.
     */
    VkShaderModule createShaderModule(const std::vector<char>& code);


    /**
     * @brief Create surface.
     */
    void createSurface();


    /**
     * @brief Create swap chain.
     */
    void createSwapChain();


    /**
     * @brief Create sync objects.
     */
    void createSyncObjects();


    /**
     * @brief Create vertex buffer.
     */
    void createVertexBuffer();


    /**
     * @brief Draw frame.
     */
    void drawFrame();


    /**
     * @brief Find the memory  type.
     * @param typeFilter Filter to process.
     * @param props Properties to process.
     * @return  Memory type value.
     */
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props);


    /**
     * @brief Find queue families.
     * @param device Device to process.
     * @return QueueFamilyIndices strcture.
     */
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);


    /**
     * @return List of required extensions.
     */
    std::vector<const char*> getRequiredExtensions();


    /**
     * Initialise Vulkan.
     */
    void initVulkan();


    /**
     * Initiase window.
     */
    void initWindow();


    /**
     * Main loop.
     */
    void mainLoop();


    /**
     * @brief Pick physical device.
     */
    void pickPhysicalDevice();


    /**
     * @brief Populate the create info.
     * @param createInfo Create info to process.
     */
    void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);


    /**
     * @brief Query swap chain support.
     * @param device Device to process.
     * @return Swap chain support details.
     */
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);


    /**
     * @brief Rate GPU devices.
     * @param device Device to process.
     * @return Rating score.
     */
    int rateDeviceSuitability(VkPhysicalDevice device);


    /**
     * @brief Recreate the swap chain.
     */
    void recreateSwapChain();


    /**
     * Setup debug messenger.
     */
    void setupDebugMessenger();


    /**
     * References.
     */
    VkDebugUtilsMessengerEXT mDebugMessenger;

    VkQueue mGraphicsQueue;
    VkPipeline mGraphicsPipeline;
    VkInstance mInstance;
    VkDevice mLogicalDevice;
    VkPipelineLayout mPipelineLayout;
    VkQueue mPresentQueue;
    VkPhysicalDevice mPhysicalDevice;
    VkRenderPass mRenderPass;
    VkSurfaceKHR mSurface;
    VkSwapchainKHR mSwapChain;
    VkExtent2D mSwapChainExtent;
    std::vector<VkFramebuffer> mSwapChainFramebuffers;
    std::vector<VkImage> mSwapChainImages;
    VkFormat mSwapchainImageFormat;
    std::vector<VkImageView> mSwapChainImageViews;
    GLFWwindow* mWindow;

    VkBuffer mVertexBuffer;
    VkDeviceMemory mVertexBufferMemory;

    std::vector<VkCommandBuffer> mCommandBuffers;
    VkCommandPool mCommandPool;

    bool mFrameBufferResized;

    std::vector<VkSemaphore> mSemaphoreImageAvailable;
    std::vector<VkSemaphore> mSemaphoreRenderFinished;
    std::vector<VkFence> mFencesInFlight;
    size_t mCurrentFrame = 0;

    const size_t MAX_FRAMES_IN_FLIGHT = 2;

};  // End class Application


#endif  // _Application_h_
