/**
 * Vulkan Tutorial
 * Ref:     https://vulkan-tutorial.com
 * Author:  T.Sang Tran
 */

// Local includes
#include "Application.h"

// STL includes
#include <iostream>


// ************************************************************
// Main
// ************************************************************
int main() {
    Application app;

    try {
        std::cout << "----------------------------------------" << std::endl;
        std::cout << "If using QtCreator, check these variables does also set." << std::endl;
#ifdef __APPLE__
        std::cout << "export VK_ICD_FILENAMES=${VULKAN_HOME}/etc/vulkan/icd.d/MoltenVK_icd.json <application>"
                  << std::endl;
#endif
        std::cout << "export VK_LAYER_PATH=${VULKAN_HOME}/etc/vulkan/explicit_layer.d <application>" << std::endl;
        std::cout << "----------------------------------------" << std::endl;
        app.run();
    } catch(const std::runtime_error& e) {
        std::cout << "---------------------" << std::endl;
        std::cout << "- Runtime Exception -" << std::endl;
        std::cout << "---------------------" << std::endl;
        std::cerr << e.what() << std::endl;
        std::cout << "---------------------" << std::endl;
        return EXIT_FAILURE;
    } catch(const std::exception& e) {
        std::cout << "-------------" << std::endl;
        std::cout << "- Exception -" << std::endl;
        std::cout << "-------------" << std::endl;
        std::cerr << e.what() << std::endl;
        std::cout << "-------------" << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
