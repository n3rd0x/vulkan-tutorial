/**
 * Vulkan Tutorial
 * Ref:     https://vulkan-tutorial.com
 * Author:  T.Sang Tran
 */
// TODO: https://vulkan-tutorial.com/en/Vertex_buffers/Staging_buffer
// Local includes
#include "Application.h"

// STL includes
#include <algorithm>
#include <cstring>
#include <map>
#include <set>
#include <vector>

#ifdef EXPLICIT_SURFACE_CREATION
// GLFW includes
#include <GLFW/glfw3native.h>
#endif


// ************************************************************
// Static Member Implementations
// ************************************************************
const std::string Application::PATH_SHADER = "Data/Assets/Shaders";

// clang-format off
const std::vector<Application::Vertex> Application::sVertices = {
    {{0.0f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f,  0.5f}, {0.0f, 1.0f, 0.0f}},
    {{-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}}
};
// clang-format on


VkResult Application::createDebugUtilsMessengerEXT(
    VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* createInfo,
    const VkAllocationCallbacks* allocator,
    VkDebugUtilsMessengerEXT* debugMessenger) {
    std::cout << "************************" << std::endl;
    std::cout << "* Create Debug Messenger" << std::endl;
    std::cout << "************************" << std::endl;
    std::cout << "Looking for process address (vkCreateDebugUtilsMessengerEXT)." << std::endl;
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    auto res  = VK_ERROR_EXTENSION_NOT_PRESENT;
    if(func != nullptr) {
        std::cout << "  - Succeeded to located the process address." << std::endl;
        res = func(instance, createInfo, allocator, debugMessenger);
    }
    if(res == VK_ERROR_EXTENSION_NOT_PRESENT) {
        std::cout << "  - Failed to locate the process address." << std::endl;
    }
    std::cout << "************************" << std::endl;
    return res;
}


void Application::destroyDebugUtilsMessengerEXT(
    VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* allocator) {
    std::cout << "Looking for process address (vkDestroyDebugUtilsMessengerEXT)." << std::endl;
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if(func != nullptr) {
        std::cout << "  - Succeeded to located the process address." << std::endl;
        func(instance, debugMessenger, allocator);
    }
    else {
        std::cout << "  - Failed to locate the process address." << std::endl;
    }
}



VKAPI_ATTR VkBool32 VKAPI_CALL Application::debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData) {
    // VkDebugReportFlagEXT:
    //      - VK_DEBUG_REPORT_INFORMATION_BIT_EXT
    //      - VK_DEBUG_REPORT_WARNING_BIT_EXT
    //      - VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT
    //      - VK_DEBUG_REPORT_ERROR_BIT_EXT
    //      - VK_DEBUG_REPORT_DEBUG_BIT_EXT
    std::cerr << "******************" << std::endl;
    std::cerr << "* Debug Messenger" << std::endl;
    std::cerr << "******************" << std::endl;
    std::cerr << "Severity: " << messageSeverity << std::endl;
    std::cerr << "Type:     " << messageType << std::endl;
    std::cerr << callbackData->pMessage << std::endl;
    std::cerr << "******************" << std::endl;

    // Should always return false.
    // Return true will mean the callback is aborted with
    // VK_ERROR_VALIDATION_FAILED_EXT. This is normally used to test the
    // validation layers themselves.
    return VK_FALSE;
}


void Application::framebufferResizedCallback(GLFWwindow* window, int width, int height) {
    auto app                 = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
    app->mFrameBufferResized = true;
}


std::vector<char> Application::readFile(const std::string& filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if(!file.is_open()) {
        throw std::runtime_error("Failed to open (" + filename + ").");
    }

    // Read size of the file to create buffer.
    size_t fileSize = static_cast<size_t>(file.tellg());
    std::vector<char> buffer(fileSize);

    // Read and save into buffer.
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}




// ************************************************************
// Member Implementations
// ************************************************************
Application::Application() {
    mPhysicalDevice = VK_NULL_HANDLE;
}


Application::~Application() {
}


bool Application::checkDeviceExtensionSupport(VkPhysicalDevice device) {
    std::cout << "********************************" << std::endl;
    std::cout << "* Check Device Extension Support" << std::endl;
    std::cout << "********************************" << std::endl;

    uint32_t count = 0;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);

    std::vector<VkExtensionProperties> availableExts(count);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &count, availableExts.data());

    std::cout << "Number of device extensions (" << count << ")" << std::endl;
    for(const auto& ext : availableExts) {
        std::cout << " * " << ext.extensionName << std::endl;
    }

    std::set<std::string> requiredExts(DEVICE_EXTENSIONS.begin(), DEVICE_EXTENSIONS.end());
    for(const char* extName : DEVICE_EXTENSIONS) {
        std::cout << "Checking " << std::string(extName) << std::endl;

        auto found = false;
        for(const auto& ext : availableExts) {
            if(strcmp(extName, ext.extensionName) == 0) {
                std::cout << "  -- Found" << std::endl;
                found = true;
                break;
            }
        }

        if(!found) {
            std::cout << "Missing required extension: " << extName << std::endl;
            return false;
        }
    }


    std::cout << "********************************" << std::endl;
    return true;
}


bool Application::checkDeviceSuitable(VkPhysicalDevice device) {
    auto indices = findQueueFamilies(device);

    auto extSupported      = checkDeviceExtensionSupport(device);
    auto swapChainAdequate = false;
    if(extSupported) {
        std::cout << "Required device extensions supported." << std::endl;
        auto swapChainSupport = querySwapChainSupport(device);
        swapChainAdequate     = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
    }
    else {
        std::cout << "Required device extensions NOT supported." << std::endl;
    }
    return indices.isComplete() && extSupported && swapChainAdequate;
}


bool Application::checkValidationLayerSupport() {
    std::cout << "********************************" << std::endl;
    std::cout << "* Check Validation Layer Support" << std::endl;
    std::cout << "********************************" << std::endl;

    uint32_t count = 0;
    vkEnumerateInstanceLayerProperties(&count, nullptr);

    std::vector<VkLayerProperties> layers(count);
    vkEnumerateInstanceLayerProperties(&count, layers.data());

    std::cout << "Number of validation layers (" << count << ")" << std::endl;
    for(const auto& l : layers) {
        std::cout << " * " << l.layerName << std::endl;
    }

    for(const char* layerName : VALIDATION_LAYERS) {
        bool found = false;
        std::cout << "Checking " << std::string(layerName) << std::endl;
        for(const auto& layer : layers) {
            if(strcmp(layerName, layer.layerName) == 0) {
                std::cout << "  -- Found" << std::endl;
                found = true;
                break;
            }
        }

        if(!found) {
            std::cout << "Missing required validation layer: " << layerName << std::endl;
            return false;
        }
    }

    std::cout << "********************************" << std::endl;
    return true;
}


VkExtent2D Application::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
    std::cout << "********************" << std::endl;
    std::cout << "* Choose Swap Extent" << std::endl;
    std::cout << "********************" << std::endl;
    if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        std::cout << "Select extent max width (" << std::numeric_limits<uint32_t>::max() << ")" << std::endl;
        return capabilities.currentExtent;
    }

    int width  = 0;
    int height = 0;
    glfwGetFramebufferSize(mWindow, &width, &height);

    VkExtent2D actualExtent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
    actualExtent.width
        = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
    actualExtent.height = std::max(
        capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

    std::cout << "Select extent (width: " << actualExtent.width << ", height: " << actualExtent.height << ")"
              << std::endl;
    return actualExtent;
}


VkPresentModeKHR Application::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availableModes) {
    std::cout << "**************************" << std::endl;
    std::cout << "* Choose Swap Present Mode" << std::endl;
    std::cout << "**************************" << std::endl;
    VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;
    bool defaultMode          = true;
    // clang-format off
    // Possible modes available:
    // VK_PRESENT_MODE_IMMEDIATE_KHR:
    //      Images submitted by your application are transferred to
    //      the screen right away, which may result in tearing.
    // VK_PRESENT_MODE_FIFO_KHR:
    //      The swap chain is a queue where the display takes an image
    //      from the front of the queue when the display is refreshed
    //      and the program inserts rendered images at the back of
    //      the queue. If the queue is full then the program has to wait.
    //      This is most similar to vertical sync as found in modern games.
    //      The moment that the display is refreshed is known as "vertical blank".
    // VK_PRESENT_MODE_FIFO_RELAXED_KHR:
    //      This mode only differs from the previous one if the application
    //      is late and the queue was empty at the last vertical blank.
    //      Instead of waiting for the next vertical blank, the image is
    //      transferred right away when it finally arrives. This may result
    //      in visible tearing.
    // VK_PRESENT_MODE_MAILBOX_KHR:
    //      This is another variation of the second mode. Instead of blocking
    //      the application when the queue is full, the images that are
    //      already queued are simply replaced with the newer ones. This mode
    //      can be used to implement triple buffering, which allows you to
    //      avoid tearing with significantly less latency issues than standard
    //      vertical sync that uses double buffering.
    // clang-format on
    for(const auto& mode : availableModes) {
        if(mode == VK_PRESENT_MODE_MAILBOX_KHR) {
            std::cout << "Select (Best) VK_PRESENT_MODE_MAILBOX_KHR" << std::endl;
            return mode;
        }
        else if(mode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
            std::cout << "Select (Next best) VK_PRESENT_MODE_IMMEDIATE_KHR" << std::endl;
            bestMode    = mode;
            defaultMode = false;
        }
    }

    if(defaultMode) {
        std::cout << "Select guaranteed mode (" << VK_PRESENT_MODE_FIFO_KHR << ")" << std::endl;
    }
    std::cout << "**************************" << std::endl;
    return bestMode;
}


VkSurfaceFormatKHR Application::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
    std::cout << "****************************" << std::endl;
    std::cout << "* Choose Swap Surface Format" << std::endl;
    std::cout << "****************************" << std::endl;

    // Using SRGB if available because it results in more accurate perceived colors.
    if(availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
        std::cout << "Selected (1): {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR}" << std::endl;
        return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }

    for(const auto& format : availableFormats) {
        if(format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            std::cout << "Select (2): {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR}" << std::endl;
            return format;
        }
    }

    std::cout << "Select first available (format: " << availableFormats[0].format
              << ", color: " << availableFormats[0].colorSpace << std::endl;
    std::cout << "****************************" << std::endl;
    return availableFormats[0];
}


void Application::cleanUp() {
    std::cout << "Cleaning up." << std::endl;
    cleanUpSwapChain();

    vkDestroyBuffer(mLogicalDevice, mVertexBuffer, nullptr);
    vkFreeMemory(mLogicalDevice, mVertexBufferMemory, nullptr);

    for(size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(mLogicalDevice, mSemaphoreRenderFinished[i], nullptr);
        vkDestroySemaphore(mLogicalDevice, mSemaphoreImageAvailable[i], nullptr);
        vkDestroyFence(mLogicalDevice, mFencesInFlight[i], nullptr);
    }
    vkDestroyCommandPool(mLogicalDevice, mCommandPool, nullptr);

    vkDestroyDevice(mLogicalDevice, nullptr);

    if(VALIDATION_SUPPORT) {
        destroyDebugUtilsMessengerEXT(mInstance, mDebugMessenger, nullptr);
    }

    vkDestroySurfaceKHR(mInstance, mSurface, nullptr);
    vkDestroyInstance(mInstance, nullptr);
    glfwDestroyWindow(mWindow);
    glfwTerminate();
    std::cout << "Cleaning up completed." << std::endl;
}


void Application::cleanUpSwapChain() {
    std::cout << "Clean up the swap chain." << std::endl;
    for(auto framebuffer : mSwapChainFramebuffers) {
        vkDestroyFramebuffer(mLogicalDevice, framebuffer, nullptr);
    }

    vkFreeCommandBuffers(
        mLogicalDevice, mCommandPool, static_cast<uint32_t>(mCommandBuffers.size()), mCommandBuffers.data());

    vkDestroyPipeline(mLogicalDevice, mGraphicsPipeline, nullptr);
    vkDestroyPipelineLayout(mLogicalDevice, mPipelineLayout, nullptr);
    vkDestroyRenderPass(mLogicalDevice, mRenderPass, nullptr);

    for(auto imageView : mSwapChainImageViews) {
        vkDestroyImageView(mLogicalDevice, imageView, nullptr);
    }
    vkDestroySwapchainKHR(mLogicalDevice, mSwapChain, nullptr);
}


void Application::createCommandBuffers() {
    std::cout << "Create command buffers." << std::endl;
    mCommandBuffers.resize(mSwapChainFramebuffers.size());

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool                 = mCommandPool;
    allocInfo.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount          = (uint32_t)mCommandBuffers.size();

    if(vkAllocateCommandBuffers(mLogicalDevice, &allocInfo, mCommandBuffers.data()) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate the command buffers!");
    }

    for(size_t i = 0; i < mCommandBuffers.size(); i++) {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        // Optional.
        beginInfo.flags            = 0;
        beginInfo.pInheritanceInfo = nullptr;

        if(vkBeginCommandBuffer(mCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
            throw std::runtime_error("Faield to begin recording command buffer (" + std::to_string(i) + ").");
        }

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType                 = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass            = mRenderPass;
        renderPassInfo.framebuffer           = mSwapChainFramebuffers[i];

        // Define the size of the render area.
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = mSwapChainExtent;

        // NB! Tripple braces just to fixed the warning
        // "suggest braces around initialization of subobject [-Wmissing-braces]"
        VkClearValue clearColor        = {{{0.0f, 0.0f, 0.0f, 1.0f}}};
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues    = &clearColor;

        // VK_SUBPASS_CONTENTS_INLINE
        //      The render pass commands will be embedded
        //      in the primary command buffer itself
        //      and no secondary command buffers will be executed.
        // VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS:
        //      The render pass commands will be executed from
        //      secondary command buffers.
        vkCmdBeginRenderPass(mCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        {
            vkCmdBindPipeline(mCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, mGraphicsPipeline);

            VkBuffer vertexBuffers[] = {mVertexBuffer};
            VkDeviceSize offsets[]   = {0};
            vkCmdBindVertexBuffers(mCommandBuffers[i], 0, 1, vertexBuffers, offsets);
            vkCmdDraw(mCommandBuffers[i], static_cast<uint32_t>(sVertices.size()), 1, 0, 0);
        }
        vkCmdEndRenderPass(mCommandBuffers[i]);

        if(vkEndCommandBuffer(mCommandBuffers[i]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to record the command buffer (" + std::to_string(i) + ").");
        }
    }
}


void Application::createCommandPool() {
    std::cout << "Create command pool." << std::endl;
    QueueFamilyIndices queueFamilyIndices = findQueueFamilies(mPhysicalDevice);

    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex        = queueFamilyIndices.graphicsFamily;
    poolInfo.flags                   = 0;

    if(vkCreateCommandPool(mLogicalDevice, &poolInfo, nullptr, &mCommandPool) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create the command pool!");
    }
}


void Application::createFramebuffers() {
    std::cout << "Create framebuffers." << std::endl;
    mSwapChainFramebuffers.resize(mSwapChainImageViews.size());

    std::cout << "Number of buffers to create: " << mSwapChainImageViews.size() << std::endl;
    for(size_t i = 0; i < mSwapChainImageViews.size(); i++) {
        VkImageView attachments[]               = {mSwapChainImageViews[i]};
        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType                   = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass              = mRenderPass;
        framebufferInfo.attachmentCount         = 1;
        framebufferInfo.pAttachments            = attachments;
        framebufferInfo.width                   = mSwapChainExtent.width;
        framebufferInfo.height                  = mSwapChainExtent.height;
        framebufferInfo.layers                  = 1;

        if(vkCreateFramebuffer(mLogicalDevice, &framebufferInfo, nullptr, &mSwapChainFramebuffers[i]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create framebuffer (" + std::to_string(i) + ")!");
        }
    }
}


void Application::createGraphicsPipeline() {
    std::cout << "Create graphic pipeline." << std::endl;
    std::cout << "Tips! Compile shader using glslangvalidator -V [srcFile] -o [outFile]." << std::endl;

    VkPipelineShaderStageCreateInfo vertInfo = {};
    auto vCode                               = readFile(PATH_SHADER + "/Basic.vert.spv");
    VkShaderModule vShader                   = createShaderModule(vCode);
    std::cout << "Successful creating the vertex shader." << std::endl;

    vertInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertInfo.stage  = VK_SHADER_STAGE_VERTEX_BIT;
    vertInfo.module = vShader;
    vertInfo.pName  = "main";

    VkPipelineShaderStageCreateInfo fragInfo = {};
    auto fCode                               = readFile(PATH_SHADER + "/Simple.frag.spv");
    VkShaderModule fShader                   = createShaderModule(fCode);
    std::cout << "Successful creating the fragment shader." << std::endl;

    fragInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragInfo.stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragInfo.module = fShader;
    fragInfo.pName  = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertInfo, fragInfo};


    // ----------------------------------------
    // "Fixed-Pipeline"
    // ----------------------------------------
    // Format of the vertex data that will be passed to the vertex shader.
    // Since we will for now hard code the vertex data, we will specify
    // that there is no vertex data to load.
    auto descBinding   = Vertex::getBindingDescription();
    auto descAttribute = Vertex::getAttributeDescription();

    VkPipelineVertexInputStateCreateInfo vertexInput = {};
    vertexInput.sType                                = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInput.vertexBindingDescriptionCount        = 1;
    vertexInput.pVertexBindingDescriptions           = &descBinding;
    vertexInput.vertexAttributeDescriptionCount      = static_cast<uint32_t>(descAttribute.size());
    vertexInput.pVertexAttributeDescriptions         = descAttribute.data();


    // Input assembly describe what kind of geometry will be drawn from
    // the vertices:
    // VK_PRIMITIVE_TOPOLOGY_POINT_LIST     -> points from vertices
    // VK_PRIMITIVE_TOPOLOGY_LINE_LIST      -> line from every 2 vertices without reuse
    // VK_PRIMITIVE_TOPOLOGY_LINE_STRIP     -> the end vertex of every line is used as
    //                                         start vertex for the next line
    // VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST  -> triangle from every 3 vertices without reuse
    // VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP -> the second and third vertex of every triangle
    //                                         are used as first two vertices of the next triangle
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology                               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable                 = VK_FALSE;


    // Define out viewport, the region of the framebuffer that the output
    // will be rendered to.
    VkViewport viewport = {};
    viewport.x          = 0.0f;
    viewport.y          = 0.0f;
    viewport.width      = static_cast<float>(mSwapChainExtent.width);
    viewport.height     = static_cast<float>(mSwapChainExtent.height);
    viewport.minDepth   = 0.0f;
    viewport.maxDepth   = 1.0f;


    // Scissor, a rectangle that define in which regions pixels will actually be stored.
    VkRect2D scissor = {};
    scissor.offset   = {0, 0};
    scissor.extent   = mSwapChainExtent;


    // Create the viewport.
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount                     = 1;
    viewportState.pViewports                        = &viewport;
    viewportState.scissorCount                      = 1;
    viewportState.pScissors                         = &scissor;


    // Takes the geometry shaped by the vertices from the vertex
    // shader and turns it into fragments to be colored by the
    // fragment shader.
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType                                  = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable                       = VK_FALSE;
    rasterizer.rasterizerDiscardEnable                = VK_FALSE;
    rasterizer.polygonMode                            = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth                              = 1.0;
    rasterizer.cullMode                               = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace                              = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable                        = VK_FALSE;

    // Optional
    rasterizer.depthBiasConstantFactor = 0.0f;
    rasterizer.depthBiasClamp          = 0.0f;
    rasterizer.depthBiasSlopeFactor    = 0.0f;


    VkPipelineMultisampleStateCreateInfo multisamling = {};
    multisamling.sType                                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisamling.sampleShadingEnable                  = VK_FALSE;
    multisamling.rasterizationSamples                 = VK_SAMPLE_COUNT_1_BIT;

    // Optinal.
    multisamling.minSampleShading      = 1.0f;
    multisamling.pSampleMask           = nullptr;
    multisamling.alphaToCoverageEnable = VK_FALSE;
    multisamling.alphaToOneEnable      = VK_FALSE;


    // How the colors should be combined with the color that
    // is already in the framebuffer.
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask
        = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;

    // Optional.
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.colorBlendOp        = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp        = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType                               = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable                       = VK_FALSE;
    colorBlending.logicOp                             = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount                     = 1;
    colorBlending.pAttachments                        = &colorBlendAttachment;

    // Optional.
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;


    // Create an empty pipeline layout.
    VkPipelineLayoutCreateInfo pipelineLayout = {};
    pipelineLayout.sType                      = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

    // Optional.
    pipelineLayout.setLayoutCount         = 0;
    pipelineLayout.pSetLayouts            = nullptr;
    pipelineLayout.pushConstantRangeCount = 0;
    pipelineLayout.pPushConstantRanges    = nullptr;

    if(vkCreatePipelineLayout(mLogicalDevice, &pipelineLayout, nullptr, &mPipelineLayout) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create pipeline layout!");
    }


    // Create the graphical pipeline.
    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount                   = 2;
    pipelineInfo.pStages                      = shaderStages;
    pipelineInfo.pVertexInputState            = &vertexInput;
    pipelineInfo.pInputAssemblyState          = &inputAssembly;
    pipelineInfo.pViewportState               = &viewportState;
    pipelineInfo.pRasterizationState          = &rasterizer;
    pipelineInfo.pMultisampleState            = &multisamling;
    pipelineInfo.pColorBlendState             = &colorBlending;
    pipelineInfo.layout                       = mPipelineLayout;
    pipelineInfo.renderPass                   = mRenderPass;
    pipelineInfo.subpass                      = 0;

    // Optional.
    pipelineInfo.pDepthStencilState = nullptr;
    pipelineInfo.pDynamicState      = nullptr;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex  = -1;


    if(vkCreateGraphicsPipelines(mLogicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &mGraphicsPipeline)
       != VK_SUCCESS) {
        throw std::runtime_error("Failed to create the graphics pipeline!");
    }


    // Clean up the shader modules.
    vkDestroyShaderModule(mLogicalDevice, vShader, nullptr);
    vkDestroyShaderModule(mLogicalDevice, fShader, nullptr);
    std::cout << "Clean up the shader modules." << std::endl;
}


void Application::createInstance() {
    std::cout << "Setup the Vulkan instance." << std::endl;

    // Validate layers.
    if(VALIDATION_SUPPORT && !checkValidationLayerSupport()) {
        throw std::runtime_error("Validation layers requested, but not available!");
    }

    // Create an application information structure.
    VkApplicationInfo appInfo  = {};
    appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName   = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName        = "No Engine";
    appInfo.engineVersion      = VK_MAKE_VERSION(0, 0, 0);
    appInfo.apiVersion         = VK_API_VERSION_1_0;

    // Create an instance information structure.
    VkInstanceCreateInfo instInfo = {};
    instInfo.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instInfo.pApplicationInfo     = &appInfo;


    // Request available extensions.
    uint32_t numOfExts = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &numOfExts, nullptr);

    std::vector<VkExtensionProperties> exts(numOfExts);
    vkEnumerateInstanceExtensionProperties(nullptr, &numOfExts, exts.data());

    std::cout << "Available extensions (" << numOfExts << ")" << std::endl;
    for(const auto& e : exts) {
        std::cout << " * " << e.extensionName << std::endl;
    }

    // Get extensions.
    auto rexts = getRequiredExtensions();
    std::cout << "Selected extensions (" << rexts.size() << ")" << std::endl;
    for(const auto e : rexts) {
        std::cout << " * " << e << std::endl;
    }

    instInfo.enabledExtensionCount   = static_cast<uint32_t>(rexts.size());
    instInfo.ppEnabledExtensionNames = rexts.data();

    // Apply the debug messenger to the extensions.
    VkDebugUtilsMessengerCreateInfoEXT debugInfo;
    if(VALIDATION_SUPPORT) {
        std::cout << "Add debug to selected extensions." << std::endl;
        instInfo.enabledLayerCount   = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        instInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();

        populateDebugMessengerCreateInfo(debugInfo);
        instInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugInfo;
    }
    else {
        instInfo.enabledLayerCount = 0;
        instInfo.pNext             = nullptr;
    }

    // Create the Vulkan.
    std::cout << "Create the Vulkan instance." << std::endl;
    VkResult res = vkCreateInstance(&instInfo, nullptr, &mInstance);
    if(res != VK_SUCCESS) {
        throw std::runtime_error("Failed to create the Vulkan instance.");
    }
    std::cout << "Successfully setup the Vulkan instance." << std::endl;
}


void Application::createImageViews() {
    std::cout << "Create image views." << std::endl;
    mSwapChainImageViews.resize(mSwapChainImages.size());

    std::cout << "Number of image view to create (" << mSwapChainImages.size() << ")" << std::endl;
    for(size_t i = 0; i < mSwapChainImages.size(); i++) {
        VkImageViewCreateInfo cinfo = {};
        cinfo.sType                 = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        cinfo.image                 = mSwapChainImages[i];
        cinfo.viewType              = VK_IMAGE_VIEW_TYPE_2D;
        cinfo.format                = mSwapchainImageFormat;

        // Default colour mapping.
        cinfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

        // Set image purpose as colour targets without any mipmapping.
        // If developing a 3D stereographic application we could create
        // a swap chain with multiple layers and image views, for each
        // presentation of the views, one for the left and one for the
        // right eyes.
        cinfo.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
        cinfo.subresourceRange.baseMipLevel   = 0;
        cinfo.subresourceRange.levelCount     = 1;
        cinfo.subresourceRange.baseArrayLayer = 0;
        cinfo.subresourceRange.layerCount     = 1;

        if(vkCreateImageView(mLogicalDevice, &cinfo, nullptr, &mSwapChainImageViews[i]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to crete image views at index (" + std::to_string(i) + ").");
        }
    }
    std::cout << "Successfully create image views." << std::endl;
}


void Application::createLogicalDevice() {
    std::cout << "Setup a logical device." << std::endl;

    auto indices = findQueueFamilies(mPhysicalDevice);

    // Create queue with graphics capabilities. Current driver
    // only allowed to create a small number of queues for each
    // queue family. There is usual no need for more than one.
    // Because there is possible to create all of the command
    // buffers on multiple threads and then submit all at once
    // on the main thread with a single low-overheaded call.
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<int> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily};

    // Priority range from 0.0 to 1.0.
    float queuePriority = 1.0f;
    for(int queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo qinfo = {};
        qinfo.sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        qinfo.queueFamilyIndex        = queueFamily;
        qinfo.queueCount              = 1;
        qinfo.pQueuePriorities        = &queuePriority;
        queueCreateInfos.push_back(qinfo);
    }

    // Specifying device features, at the moment we will skip this.
    // List of features was already demonstrated in "pickPhysicalDevice".
    VkPhysicalDeviceFeatures deviceFeatures = {};

    // Create the logical device.
    VkDeviceCreateInfo cinfo      = {};
    cinfo.sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    cinfo.queueCreateInfoCount    = static_cast<uint32_t>(queueCreateInfos.size());
    cinfo.pQueueCreateInfos       = queueCreateInfos.data();
    cinfo.pEnabledFeatures        = &deviceFeatures;
    cinfo.enabledExtensionCount   = static_cast<uint32_t>(DEVICE_EXTENSIONS.size());
    cinfo.ppEnabledExtensionNames = DEVICE_EXTENSIONS.data();
    if(VALIDATION_SUPPORT) {
        cinfo.enabledLayerCount   = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        cinfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
    }
    else {
        cinfo.enabledLayerCount = 0;
    }

    if(vkCreateDevice(mPhysicalDevice, &cinfo, nullptr, &mLogicalDevice) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create logical device.");
    }

    // Get handle to the graphics queue. Since there is only one queue that
    // has been create, the index is set to 0.
    vkGetDeviceQueue(mLogicalDevice, indices.graphicsFamily, 0, &mGraphicsQueue);
    vkGetDeviceQueue(mLogicalDevice, indices.presentFamily, 0, &mPresentQueue);

    std::cout << "Successfully setup a physical device." << std::endl;
}


void Application::createRenderPass() {
    std::cout << "Setup the render pass." << std::endl;

    // Define how we would like the colors to be load and stored.
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format                  = mSwapchainImageFormat;
    colorAttachment.samples                 = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp                  = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp                 = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp           = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp          = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout           = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout             = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment            = 0;
    colorAttachmentRef.layout                = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


    // Define the subpass.
    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint    = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments    = &colorAttachmentRef;

    VkSubpassDependency dependency = {};
    dependency.srcSubpass          = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass          = 0;
    dependency.srcStageMask        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask       = 0;
    dependency.dstStageMask        = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask       = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    // Create the render pass.
    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType                  = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount        = 1;
    renderPassInfo.pAttachments           = &colorAttachment;
    renderPassInfo.subpassCount           = 1;
    renderPassInfo.pSubpasses             = &subpass;
    renderPassInfo.dependencyCount        = 1;
    renderPassInfo.pDependencies          = &dependency;

    if(vkCreateRenderPass(mLogicalDevice, &renderPassInfo, nullptr, &mRenderPass) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create the render pass!");
    }
    std::cout << "Successfully setup the render pass." << std::endl;
}


VkShaderModule Application::createShaderModule(const std::vector<char>& code) {
    VkShaderModuleCreateInfo cinfo = {};
    cinfo.sType                    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    cinfo.codeSize                 = code.size();
    cinfo.pCode                    = reinterpret_cast<const uint32_t*>(code.data());

    VkShaderModule shaderModule;
    if(vkCreateShaderModule(mLogicalDevice, &cinfo, nullptr, &shaderModule) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create shader module.");
    }

    return shaderModule;
}


void Application::createSurface() {
    std::cout << "Setup the surface." << std::endl;
#ifndef EXPLICIT_SURFACE_CREATION
    if(glfwCreateWindowSurface(mInstance, mWindow, nullptr, &mSurface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create a surface.");
    }
#else
    // For learning purpose only, to see how glfwCreateWindowSurface
    // is implemented behind the scene.
    // NB! It's required to use ".mm" extension.
#if defined(VK_USE_PLATFORM_MACOS_MVK)
    NSWindow* nsWindow = glfwGetCocoaWindow(mWindow);
    if(!nsWindow) {
        throw std::runtime_error("Can't accessing the NSWindow.");
    }

    NSView* nsView = nsWindow.contentView;
    if(!nsView) {
        throw std::runtime_error("Can't accessing the NSView.");
    }

    VkMacOSSurfaceCreateInfoMVK cinfo = {};
    cinfo.sType                       = VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK;
    cinfo.pView                       = nsView;

    std::cout << "Locating process address (vkCreateMacOSSurfaceMVK)" << std::endl;
    auto vkCreateMacOSSurfaceMVK
        = (PFN_vkCreateMacOSSurfaceMVK)vkGetInstanceProcAddr(mInstance, "vkCreateMacOSSurfaceMVK");
    if(!vkCreateMacOSSurfaceMVK) {
        throw std::runtime_error("Failed to locate process address (vkCreateMacOSSurfaceMVK)");
    }

    std::cout << "  - Succeeded to located the process address." << std::endl;
    if(vkCreateMacOSSurfaceMVK(mInstance, &cinfo, nullptr, &mSurface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create a MacOS surface.");
    }
    std::cout << "Succeeded to create a MacOS surface." << std::endl;
#elif defined(VK_USE_PLATFORM_WIN32_KHR)
    // NB! Not tested.
    VkWin32SurfaceCreateInfoKHR cinfo = {};
    cinfo.sType                       = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    cinfo.hwnd                        = glfwGetWin32Window(mWindow);
    cinfo.hinstance                   = GetModuleHandle(nullptr);

    std::cout << "Locating process address (vkCreateWin32SurfaceKHR)" << std::endl;
    auto vkCreateWin32SurfaceKHR
        = (PFN_vkCreateWin32SurfaceKHR)vkGetInstanceProcAddr(mInstance, "vkCreateWin32SurfaceKHR");
    if(!vkCreateWin32SurfaceKHR) {
        throw std::runtime_error("Failed to locate process address (vkCreateWin32SurfaceKHR)");
    }

    std::cout << "  - Succeeded to located the process address." << std::endl;
    if(vkCreateWin32SurfaceKHR(mInstance, &cinfo, nullptr, &mSurface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create Win32 surface.");
    }
    std::cout << "Succeeded to create a Win32 surface." << std::endl;
#endif
#endif
    std::cout << "Successfully setup the surface." << std::endl;
}


void Application::createSwapChain() {
    std::cout << "Setup the swap chain." << std::endl;
    auto swapchainSupport = querySwapChainSupport(mPhysicalDevice);
    auto surfaceFormat    = chooseSwapSurfaceFormat(swapchainSupport.formats);
    auto presentMode      = chooseSwapPresentMode(swapchainSupport.presentModes);
    auto extent           = chooseSwapExtent(swapchainSupport.capabilities);

    // Checking for triple buffer available.
    // NB! If maxImageCount is 0, means there is no limit besides memory requirments.
    uint32_t imageCount = swapchainSupport.capabilities.minImageCount + 1;

    std::cout << "Checking for number of image support: " << imageCount << std::endl;
    if(swapchainSupport.capabilities.maxImageCount > 0 && imageCount > swapchainSupport.capabilities.maxImageCount) {
        imageCount = swapchainSupport.capabilities.maxImageCount;
    }
    std::cout << "Available image support: " << imageCount << std::endl;

    // Creating the swap chain. The "imageArrayLayers" specifies the amount of layers
    // each image consists of. This is always 1, unless developing steraoscopic 3D.
    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType                    = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface                  = mSurface;
    createInfo.minImageCount            = imageCount;
    createInfo.imageFormat              = surfaceFormat.format;
    createInfo.imageColorSpace          = surfaceFormat.colorSpace;
    createInfo.imageExtent              = extent;
    createInfo.imageArrayLayers         = 1;
    createInfo.imageUsage               = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    std::cout << "Checking sharing mode...." << std::endl;
    auto indices = findQueueFamilies(mPhysicalDevice);
    uint32_t queueFamiltyIndices[]
        = {static_cast<uint32_t>(indices.graphicsFamily), static_cast<uint32_t>(indices.presentFamily)};
    if(indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode      = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices   = queueFamiltyIndices;
        std::cout << "Mode selected: VK_SHARING_MODE_CONCURRENT" << std::endl;
    }
    else {
        createInfo.imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;        // Optional.
        createInfo.pQueueFamilyIndices   = nullptr;  // Optional.
        std::cout << "Mode selected: VK_SHARING_MODE_EXCLUSIVE (best)" << std::endl;
    }

    createInfo.preTransform   = swapchainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode    = presentMode;
    createInfo.clipped        = VK_TRUE;
    createInfo.oldSwapchain   = VK_NULL_HANDLE;

    if(vkCreateSwapchainKHR(mLogicalDevice, &createInfo, nullptr, &mSwapChain) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create swap chain.");
    }
    std::cout << "Successfully setup the swap chain." << std::endl;

    vkGetSwapchainImagesKHR(mLogicalDevice, mSwapChain, &imageCount, nullptr);
    mSwapChainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(mLogicalDevice, mSwapChain, &imageCount, mSwapChainImages.data());

    mSwapchainImageFormat = surfaceFormat.format;
    mSwapChainExtent      = extent;
}


void Application::createSyncObjects() {
    std::cout << "Setup the synchronisation objects." << std::endl;
    mSemaphoreImageAvailable.resize(MAX_FRAMES_IN_FLIGHT);
    mSemaphoreRenderFinished.resize(MAX_FRAMES_IN_FLIGHT);
    mFencesInFlight.resize(MAX_FRAMES_IN_FLIGHT);


    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags             = VK_FENCE_CREATE_SIGNALED_BIT;

    for(size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if((vkCreateSemaphore(mLogicalDevice, &semaphoreInfo, nullptr, &mSemaphoreImageAvailable[i]) != VK_SUCCESS)
           || (vkCreateSemaphore(mLogicalDevice, &semaphoreInfo, nullptr, &mSemaphoreRenderFinished[i]) != VK_SUCCESS)
           || (vkCreateFence(mLogicalDevice, &fenceInfo, nullptr, &mFencesInFlight[i]))) {
            throw std::runtime_error("Failed to create synchronisation objects for a frame!");
        }
    }

    std::cout << "Successfully setup the synchronisation objects." << std::endl;
}


void Application::createVertexBuffer() {
    std::cout << "Setup the vertex buffer." << std::endl;

    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType              = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size               = sizeof(sVertices[0]) * sVertices.size();
    bufferInfo.usage              = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    bufferInfo.sharingMode        = VK_SHARING_MODE_EXCLUSIVE;

    if(vkCreateBuffer(mLogicalDevice, &bufferInfo, nullptr, &mVertexBuffer) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create vertex buffer!");
    }

    VkMemoryRequirements memReq;
    vkGetBufferMemoryRequirements(mLogicalDevice, mVertexBuffer, &memReq);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType                = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize       = memReq.size;
    allocInfo.memoryTypeIndex      = findMemoryType(
        memReq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    if(vkAllocateMemory(mLogicalDevice, &allocInfo, nullptr, &mVertexBufferMemory) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate vertex buffer memory!");
    }

    vkBindBufferMemory(mLogicalDevice, mVertexBuffer, mVertexBufferMemory, 0);

    // Fill the data.
    void* data;
    vkMapMemory(mLogicalDevice, mVertexBufferMemory, 0, bufferInfo.size, 0, &data);
    memcpy(data, sVertices.data(), (size_t)bufferInfo.size);
    vkUnmapMemory(mLogicalDevice, mVertexBufferMemory);


    std::cout << "Successfully setup the vertex buffer." << std::endl;
}


void Application::drawFrame() {
    vkWaitForFences(mLogicalDevice, 1, &mFencesInFlight[mCurrentFrame], VK_TRUE, UINT64_MAX);
    vkResetFences(mLogicalDevice, 1, &mFencesInFlight[mCurrentFrame]);

    uint32_t imgIndex;
    VkResult result = vkAcquireNextImageKHR(
        mLogicalDevice, mSwapChain, UINT64_MAX, mSemaphoreImageAvailable[mCurrentFrame], VK_NULL_HANDLE, &imgIndex);

    if(result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain();
        return;
    }
    else if((result != VK_SUCCESS) && (result != VK_SUBOPTIMAL_KHR)) {
        throw std::runtime_error("Failed to aquire swap chain image.");
    }

    VkSemaphore waitSemaphores[]      = {mSemaphoreImageAvailable[mCurrentFrame]};
    VkSemaphore signalSemaphores[]    = {mSemaphoreRenderFinished[mCurrentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    VkSwapchainKHR swapChains[]       = {mSwapChain};

    VkSubmitInfo submitInfo         = {};
    submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount   = 1;
    submitInfo.pWaitSemaphores      = waitSemaphores;
    submitInfo.pWaitDstStageMask    = waitStages;
    submitInfo.commandBufferCount   = 1;
    submitInfo.pCommandBuffers      = &mCommandBuffers[imgIndex];
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores    = signalSemaphores;

    vkResetFences(mLogicalDevice, 1, &mFencesInFlight[mCurrentFrame]);
    if(vkQueueSubmit(mGraphicsQueue, 1, &submitInfo, mFencesInFlight[mCurrentFrame]) != VK_SUCCESS) {
        throw std::runtime_error("Failed to submit draw command buffer!");
    }

    VkPresentInfoKHR presentInfo   = {};
    presentInfo.sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores    = signalSemaphores;
    presentInfo.swapchainCount     = 1;
    presentInfo.pSwapchains        = swapChains;
    presentInfo.pImageIndices      = &imgIndex;

    // Optional
    presentInfo.pResults = nullptr;

    result = vkQueuePresentKHR(mPresentQueue, &presentInfo);
    if((result == VK_ERROR_OUT_OF_DATE_KHR) || (result == VK_SUBOPTIMAL_KHR) || mFrameBufferResized) {
        mFrameBufferResized = false;
        recreateSwapChain();
    }
    else if(result != VK_SUCCESS) {
        throw std::runtime_error("Failed to present the swap chain image.");
    }
    mCurrentFrame = (mCurrentFrame + 1) & MAX_FRAMES_IN_FLIGHT;
}


uint32_t Application::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props) {
    std::cout << "Find a suitable memory type." << std::endl;

    VkPhysicalDeviceMemoryProperties memProps;
    vkGetPhysicalDeviceMemoryProperties(mPhysicalDevice, &memProps);
    for(uint32_t i = 0; i < memProps.memoryTypeCount; i++) {
        if((typeFilter & (1 << i)) && ((memProps.memoryTypes[i].propertyFlags & props) == props)) {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable memory type!");
}


Application::QueueFamilyIndices Application::findQueueFamilies(VkPhysicalDevice device) {
    std::cout << "***********************" << std::endl;
    std::cout << "* Retrieve Queue Family" << std::endl;
    std::cout << "***********************" << std::endl;
    QueueFamilyIndices indices;

    // Get number of queue family.
    uint32_t counts = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &counts, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(counts);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &counts, queueFamilies.data());

    std::cout << "Number of queue families (" << counts << ")" << std::endl;
    auto i = 0;
    for(const auto& queueFamily : queueFamilies) {
        if(queueFamily.queueCount == 0) {
            std::cout << "  - No queue count, continue." << std::endl;
            break;
        }

        if(queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphicsFamily = i;
            std::cout << "  * Queue (Graphics) supported." << std::endl;
        }

        if(queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT) {
            std::cout << "  * Queue (Compute) supported." << std::endl;
        }

        if(queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT) {
            std::cout << "  * Queue (Transfer) supported." << std::endl;
        }

        if(queueFamily.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) {
            std::cout << "  * Queue (Sparse Binding) supported." << std::endl;
        }

        if(queueFamily.queueFlags & VK_QUEUE_PROTECTED_BIT) {
            std::cout << "  * Queue (Protected) supported." << std::endl;
        }

        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, mSurface, &presentSupport);
        if(presentSupport) {
            indices.presentFamily = i;
            std::cout << "  * Queue (Present) supported." << std::endl;
        }


        if(indices.isComplete()) {
            break;
        }

        i++;
    }

    std::cout << "***********************" << std::endl;
    return indices;
}


std::vector<const char*> Application::getRequiredExtensions() {
    std::cout << "******************************" << std::endl;
    std::cout << "* Retrieve Required Extensions" << std::endl;
    std::cout << "******************************" << std::endl;
    std::vector<const char*> exts;

    uint32_t glfwExtsCount = 0;
    const char** glfwExts  = glfwGetRequiredInstanceExtensions(&glfwExtsCount);

    std::cout << "Number of required extensions in GLFW: " << glfwExtsCount << std::endl;
    for(uint32_t i = 0; i < glfwExtsCount; i++) {
        exts.push_back(glfwExts[i]);
        std::cout << " * " << exts.back() << std::endl;
    }

    if(VALIDATION_SUPPORT) {
        // Real name: VK_EXT_debug_utils
        exts.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        std::cout << "Adding " << exts.back() << std::endl;
    }

    std::cout << "******************************" << std::endl;
    return exts;
}


void Application::initVulkan() {
    std::cout << "Setup Vulkan." << std::endl;
    if(!glfwVulkanSupported()) {
        throw std::runtime_error("GLFW doesn't support Vulkan.");
    }

    createInstance();
    setupDebugMessenger();
    createSurface();
    pickPhysicalDevice();
    createLogicalDevice();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandPool();
    createVertexBuffer();
    createCommandBuffers();
    createSyncObjects();

    std::cout << "Successfully setup Vulkan ^_^" << std::endl;
}


void Application::initWindow() {
    std::cout << "Setup the GLFW window (" << WINDOW_WIDTH << "x" << WINDOW_HEIGHT << ")." << std::endl;

    glfwInit();

    // Don't let GLFW create an OpenGL context at the moment,
    // and disable resizing.
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    // Create the window.
    mWindow = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Vulkan", nullptr, nullptr);
    glfwSetWindowUserPointer(mWindow, this);
    glfwSetFramebufferSizeCallback(mWindow, framebufferResizedCallback);
    std::cout << "Successfully setup the GLFW window." << std::endl;
}


void Application::mainLoop() {
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "- START MainLoop" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    while(!glfwWindowShouldClose(mWindow)) {
        glfwPollEvents();
        drawFrame();
    }

    vkDeviceWaitIdle(mLogicalDevice);
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "- STOP  MainLoop" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
}


void Application::pickPhysicalDevice() {
    std::cout << "****************************" << std::endl;
    std::cout << "* Retrieve Available Devices" << std::endl;
    std::cout << "****************************" << std::endl;

    uint32_t numOfDevs = 0;
    vkEnumeratePhysicalDevices(mInstance, &numOfDevs, nullptr);
    if(numOfDevs == 0) {
        throw std::runtime_error("Failed to find GPU with Vulkan support.");
    }

    std::vector<VkPhysicalDevice> devices(numOfDevs);
    vkEnumeratePhysicalDevices(mInstance, &numOfDevs, devices.data());

    // Ranking devices. Using an ordered map to automatically sort
    // candidates by scores. This is for demonstration how to pick
    // a device.
    std::multimap<int, VkPhysicalDevice> candidates;
    std::cout << "Available devices (" << numOfDevs << ")" << std::endl;
    if(numOfDevs > 0) {
        std::cout << "**** Physical Devices ****" << std::endl;
        for(const auto& device : devices) {
            // Get device properties.
            VkPhysicalDeviceProperties props;
            vkGetPhysicalDeviceProperties(device, &props);

            // Get device features.
            VkPhysicalDeviceFeatures features;
            vkGetPhysicalDeviceFeatures(device, &features);

            // clang-format off
            std::cout << "  " << "--------------------" << std::endl;
            std::cout << "  " << "- " << props.deviceName << std::endl;
            std::cout << "  " << "--------------------" << std::endl;
            std::cout << "  " << "Device ID:      " << props.deviceID << std::endl;
            std::cout << "  " << "Device Type:    " << props.deviceType << std::endl;
            std::cout << "  " << "API Version:    " << props.apiVersion << std::endl;
            std::cout << "  " << "Driver Version: " << props.driverVersion << std::endl;
            std::cout << "  " << "Vendor ID:      " << props.vendorID << std::endl;
            std::cout << "  " << "------------" << std::endl;
            std::cout << "  " << "- Features -" << std::endl;
            std::cout << "  " << "------------" << std::endl;
            std::cout << "  " << "Geometry:       " << features.geometryShader << std::endl;
            std::cout << "  " << "--------------------" << std::endl;
            // clang-format on

            int score = rateDeviceSuitability(device);
            candidates.insert(std::make_pair(score, device));
        }
        std::cout << "**** End Physical Devices **** " << std::endl;
    }

    // List ranking.
    if(!candidates.empty()) {
        std::cout << "Ranking devices:" << std::endl;
        for(const auto& candidate : candidates) {
            auto score = candidate.first;
            auto dev   = candidate.second;

            VkPhysicalDeviceProperties props;
            vkGetPhysicalDeviceProperties(dev, &props);
            std::cout << props.deviceName << " (" << score << ")" << std::endl;
        }
    }


    for(const auto& device : devices) {
        if(checkDeviceSuitable(device)) {
            mPhysicalDevice = device;
            break;
        }
    }

    if(mPhysicalDevice == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to find a suitable GPU device.");
    }


    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(mPhysicalDevice, &props);
    std::cout << "***************" << std::endl;
    std::cout << "* Selected GPU: " << props.deviceName << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << "****************************" << std::endl;
}


void Application::populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    std::cout << "Populate the debug messenger create info." << std::endl;

    createInfo                 = {};
    createInfo.sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
                                 | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                                 | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
                             | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
                             | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = debugCallback;
}


Application::SwapChainSupportDetails Application::querySwapChainSupport(VkPhysicalDevice device) {
    std::cout << "**************************" << std::endl;
    std::cout << "* Query Swap Chain Support" << std::endl;
    std::cout << "**************************" << std::endl;
    SwapChainSupportDetails details;

    // Query surface capabilities.
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, mSurface, &details.capabilities);

    // Query surface formats.
    uint32_t formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, mSurface, &formatCount, nullptr);

    std::cout << "Available surface formats (" << formatCount << ")" << std::endl;
    if(formatCount != 0) {
        details.formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, mSurface, &formatCount, details.formats.data());
    }

    // Query surface present mode.
    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, mSurface, &presentModeCount, nullptr);

    std::cout << "Available surce present modes (" << presentModeCount << ")" << std::endl;
    if(presentModeCount != 0) {
        details.presentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, mSurface, &presentModeCount, details.presentModes.data());
    }

    std::cout << "**************************" << std::endl;
    return details;
}


int Application::rateDeviceSuitability(VkPhysicalDevice device) {
    auto score = 0;

    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(device, &props);

    VkPhysicalDeviceFeatures features;
    vkGetPhysicalDeviceFeatures(device, &features);

    // Discrete GPUs have a significant performace advantage.
    if(props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
        score += 1000;
    }

    // Maximum possible size of textures affects graphics quality.
    score += props.limits.maxImageDimension2D;

    // Application can't function without geometry shaders.
    if(!features.geometryShader) {
        return 0;
    }

    return score;
}


void Application::recreateSwapChain() {
    // In case where the window is minizing, the frame buffer size would
    // be zero. So we will handle this by pausing until the active in
    // the foreground again.
    int width  = 0;
    int height = 0;
    while(width == 0 || height == 0) {
        glfwGetFramebufferSize(mWindow, &width, &height);
        glfwWaitEvents();
    }

    std::cout << "Recreate the swap chain." << std::endl;
    vkDeviceWaitIdle(mLogicalDevice);

    cleanUpSwapChain();

    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandBuffers();
}


void Application::run() {
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "- Vulkan Application" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Validation support: ";
    if(VALIDATION_SUPPORT) {
        std::cout << "ENABLED" << std::endl;
    }
    else {
        std::cout << "DISABLED" << std::endl;
    }
    initWindow();
    initVulkan();
    mainLoop();
    cleanUp();
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "- Finished" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
}


void Application::setupDebugMessenger() {
    std::cout << "Setup the debug messenger." << std::endl;
    if(!VALIDATION_SUPPORT) {
        return;
    }

    VkDebugUtilsMessengerCreateInfoEXT createInfo;
    populateDebugMessengerCreateInfo(createInfo);

    if(createDebugUtilsMessengerEXT(mInstance, &createInfo, nullptr, &mDebugMessenger) != VK_SUCCESS) {
        throw std::runtime_error("Failed to set up debug messenger!");
    }
    std::cout << "Successfully setup the debug messenger." << std::endl;
}



// ************************************************************
// Structure Implementations
// ************************************************************
std::array<VkVertexInputAttributeDescription, 2> Application::Vertex::getAttributeDescription() {
    std::array<VkVertexInputAttributeDescription, 2> desc = {};

    // Binding the position.
    desc[0].binding  = 0;
    desc[0].location = 0;
    desc[0].format   = VK_FORMAT_R32G32_SFLOAT;
    desc[0].offset   = offsetof(Vertex, position);

    // Binding the colour.
    desc[1].binding  = 0;
    desc[1].location = 1;
    desc[1].format   = VK_FORMAT_R32G32B32_SFLOAT;
    desc[1].offset   = offsetof(Vertex, colour);
    return desc;
}

VkVertexInputBindingDescription Application::Vertex::getBindingDescription() {
    VkVertexInputBindingDescription desc = {};

    desc.binding   = 0;
    desc.stride    = sizeof(Vertex);
    desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    return desc;
}
